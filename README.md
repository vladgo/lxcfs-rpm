### Build lxcfs for CentOS 7

Lxcfs implements a FUSE fs which allows containers to have virtualized cgroup filesystems and virtualized views of /proc/cpuinfo and /proc/meminfo.

See detail at https://linuxcontainers.org/it/lxcfs/introduction/

This project build rpm package  for CentOS 7.

Mount this volumes in docker
```yaml
   - /var/lib/lxcfs/proc/meminfo:/proc/meminfo
   - /var/lib/lxcfs/proc/uptime:/proc/uptime
   - /var/lib/lxcfs/proc/swaps:/proc/swaps
   - /var/lib/lxcfs/proc/cpuinfo:/proc/cpuinfo
   - /var/lib/lxcfs/proc/stat:/proc/stat
   - /var/lib/lxcfs/proc/diskstats:/proc/diskstats
```

Usage example for docker (in russian): https://habrahabr.ru/post/324918/